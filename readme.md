![](http://us.123rf.com/450wm/patrimonio/patrimonio1605/patrimonio160500183/57153066-illustration-of-a-plasterer-masonry-tradesman-construction-worker-wearing-hat-holding-trowel-viewed-.jpg?ver=6)

# Présentation

Maconnery permet de créer des tuiles d'une taille équivalente en respectant la courbure de la Terre

# Installation

```
npm install https://bitbucket.org/nunkiBack/maconnery
```

# Utilisation

La classe Maconnery fonctionne seulement avec le format [GeoJSON](http://geojson.org/). 

```javascript
var Maconnery = require('maconnery');
var GeoJSON = require('<GeoJSON>'); // <Feature.Polygon>

// On veut créer des tuiles de 5km de côtés
var tiles = Maconnery(GeoJSON, 5000);
```