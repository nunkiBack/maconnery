/**
 * @AUTHOR: Marques Kevin
 * @COMPANY: Nunki
 * @NODE: 4.6.*
 * @DESCRIPTION: Permet de construire des tuiles depuis des pays ou des regions
 */

var Grid   = require('@turf/square-grid'), // Permet de creer
    Inside = require('@turf/inside'), // Permet de tester si un point est dans un polygone
    Circle = require('@turf/circle'),
    Point  = require('@turf/helpers').point;

/**
 * Permet de créer des rectangles à partir d'une forme geographique
 *
 * @param {array} rectangle - [minY, minX, maxY, maxX]
 * @param {number} distance - La taille maximale du cote du carre (7000m est la distance pour avoir une diagonale de 10km)
 * @constructor
 */
function Maconnery(polygon, distance) {
  var rectangle = polygonToRectangle(polygon),
      toArray   = [ rectangle.minY, rectangle.minX, rectangle.maxY, rectangle.maxX ],
      grid      = Grid(toArray, distance, 'meters');

  return squareInPolygon(grid, polygon);
}

/**
 * Transforme un polygone en rectangle pour pouvoir utiliser turf-square-grid
 *
 * @param {GeoJSON} polygon - Le polygone à transformer en rectangle
 */
function polygonToRectangle(polygon) {
  var coordinates = [];
  var square      = { minY: undefined, minX: undefined, maxY: undefined, maxY: undefined };

  if (!polygon) throw new Error('No polygon is provided');
  if (polygon.type == 'Feature') coordinates = polygon.geometry.coordinates;
  else if (polygon.type == 'Polygon') coordinates = polygon.coordinates;
  else throw new Error('is not a valid polygon');

  var square = coordinates[ 0 ].reduce(function (accumulator, value, index) {
    var lat = value[ 1 ],
        lng = value[ 0 ];

    accumulator.minY = accumulator.minY <= lng ? accumulator.minY : lng;
    accumulator.maxY = accumulator.maxY >= lng ? accumulator.maxY : lng;
    accumulator.minX = accumulator.minX <= lat ? accumulator.minX : lat;
    accumulator.maxX = accumulator.maxX >= lat ? accumulator.maxX : lat;

    return accumulator;
  }, square);

  return square;
}

/**
 * Permet de supprimer les tuiles qui sont en dehors du polygone
 *
 * @param {array} squares - La liste des carrés
 * @param {GeoJSON} polygon - Le polygon modele
 */
function squareInPolygon(squares, polygon) {
  var filter = squares.features.filter(function (value) {
    if (value.geometry.type !== "Polygon") return false;
    return value.geometry.coordinates[ 0 ].reduce(function (accumulator, value) {
      if (accumulator) return true;
      return Inside({ type: 'Point', coordinates: value }, polygon);
    }, false);
  });

  return filter;
}

/**
 * Permet de transformer un cercle en polygone
 *
 * @param {Object} latLng - La latitude et la longitude du cercle
 * @param {number} radius - Le rayon du cercle
 */
function circleToPolygon(latLng, radius) {
  var center = Point([ latLng.lng, latLng.lat ]);
  var units  = 'kilometers';
  var steps  = 4;
  var radius = radius / 1000; // Conversion en metre

  return Circle(center, radius, steps, units);
}

/**
 * Pour pouvoir utiliser et tester les fonctions en dehors de Maconnery
 */
Maconnery.utils = {
  polygonToRectangle: polygonToRectangle,
  squareInPolygon   : squareInPolygon,
  circleToPolygon   : circleToPolygon
};

module.exports = Maconnery;