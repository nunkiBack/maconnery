var expect       = require('chai').expect,
    Paris        = require('./../mocks/paris.json'),
    SquaresParis = require('./../mocks/paris.squares.json'),
    Maconnery    = require('./main');

describe('Fonctions privées', function () {
  describe('PolygonToRectangle', function () {
    it('Doit créer un rectangle à partir d\'une Feature', function () {
      var rectangle = Maconnery.utils.polygonToRectangle(Paris);
      expect(rectangle.minY < rectangle.maxY).to.be.ok;
      expect(rectangle.minX < rectangle.maxX).to.be.ok;
    });

    it('Doit renvoyer un rectangle à partir d\'un Polygon', function () {
      var rectangle = Maconnery.utils.polygonToRectangle(Paris.geometry);
      expect(rectangle.minY < rectangle.maxY).to.be.ok;
      expect(rectangle.minX < rectangle.maxX).to.be.ok;
    });

    it('Doit renvoyer une erreur si pas de polygone', function () {
      expect(Maconnery.utils.polygonToRectangle).to.throw('No polygon is provided');
    });

    it('Doit renvoyer une erreur si mauvais GeoJSON', function () {
      var args = [ { type: 'Point' } ];
      expect(Maconnery.utils.polygonToRectangle.bind(this, args)).to.throw('is not a valid polygon');
    });
  });

  describe('squareInPolygon', function () {
    it('Doit filtrer les polygon qui sont dans la zone', function () {
      var rectangle = Maconnery.utils.squareInPolygon(SquaresParis, Paris);
      expect(rectangle.length == 9).to.be.ok;
    });
    it('Doit retourner aucun rectangle', function () {
      var rects     = { features: [ { geometry: { type: 'Point' } } ] };
      var rectangle = Maconnery.utils.squareInPolygon(rects, Paris);
      expect(rectangle.length == 0).to.be.ok;
    });
  });

  describe('circleToPolygon', function () {
    it('Doit créer un cercle', function () {
      var polygon = Maconnery.utils.circleToPolygon({ lat: 48.86110101269274, lng: 2.344207763671875 }, 3000);
      expect(polygon.type == 'Feature');
      expect(polygon.geometry.type == 'Polygon');
    });
  });
});

describe('Coeur', function () {
  it('Maconnery', function () {
    var rectangle = Maconnery(Paris, 7000);
    expect(rectangle.length == 6).to.be.ok;
  });
});